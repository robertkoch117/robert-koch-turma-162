import java.util.InputMismatchException;
import java.util.Scanner;

/**
 * 
 * @author Robert_Koch_2017101788
 * @version 1.0
 */
public class Ex2ComFuncao {
	static double nota1, nota2, nota3=0;
	
	public static double calcMedia(double n1, double n2) {
		return (n1 + n2) / 2;
	}
	
	public static void main(String[] args) {
		try {
			double media;
			Scanner sc = new Scanner(System.in);
			
			System.out.print("Insira a primeira nota: ");
			nota1 = sc.nextDouble();
			System.out.print("\nInsira a segunda nota: ");
			nota2 = sc.nextDouble();
			media = calcMedia(nota1, nota2);
			
			if(media>=3 && media<=6){
				System.out.println("Voc� precisa fazer a avalia��o optativa.");
				System.out.println("Nota da optativa: ");
				nota3 = sc.nextDouble();
			}
			
			if(nota3>nota1){
				media = calcMedia(nota3,nota2);
			}else if(nota3>nota2){
				media = calcMedia(nota3,nota1);
			}else {
				media = calcMedia(nota1,nota2);
			}
				
			if(media>=6) {
				System.out.println("Aprovado.");
			}else{
				System.out.println("Reprovado.");
			}
			sc.close();
		} catch (InputMismatchException x) {
			System.out.println("Digite apenas n�meros.");
		}

	}

}
