import java.util.Scanner;
import java.util.InputMismatchException;
/**
 * 
 * @author Robert_Koch 2017101788
 * @version 1.0
 *
 */

public class Ex2 {
	public static void main(String[] args) {
		//Exercicio 2 - Selecao
		try {
			double nota1, nota2, nota3=0, media;
			Scanner sc = new Scanner(System.in);
			
			System.out.print("Insira a primeira nota: ");
			nota1 = sc.nextDouble();
			System.out.print("\nInsira a segunda nota: ");
			nota2 = sc.nextDouble();
			media = (nota1 + nota2) / 2;
			
			if(media>=3 && media<=6){
				System.out.println("Voc� precisa fazer a avalia��o optativa.");
				System.out.println("Nota da optativa: ");
				nota3 = sc.nextDouble();
			}
			
			if(nota3>nota1){
				media = (nota3+nota2)/2;
			}else if(nota3>nota2){
				media = (nota3+nota1)/2;
			}else {
				media = (nota1+nota2)/2;
			}
				
			if(media>=6) {
				System.out.println("Aprovado.");
			}else{
				System.out.println("Reprovado.");
			}
			sc.close();
		} catch (InputMismatchException x) {
			System.out.println("Digite apenas n�meros.");
		}	
	}
}
