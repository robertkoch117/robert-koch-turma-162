import java.util.InputMismatchException;
import java.util.Scanner;

/**
 * 
 * @author Robert_Koch_2017101788
 * @version 1.0
 *
 */
public class Ex4ComFuncao {
	static double av1, av2, media;
	
	public static void mensagem() {
		if(media>=6) {
			System.out.println("PARAB�NS! Voc� foi aprovado!");
		}else {
			System.out.println("Voc� foi REPROVADO! Estude mais...");
		}
	}
	
	public static double calcMedia(double n1, double n2) {
		return (n1+n2)/2;
	}
	
	public static void main(String[] args) {
		try {
			
			Scanner sc = new Scanner(System.in);
			
			System.out.print("Escreva a primeira nota: ");
			av1 = sc.nextInt();
			System.out.print("Escreva a segunda nota: ");
			av2 = sc.nextInt();
			media = calcMedia(av1,av2);
			
			mensagem();
			
			sc.close();
		}catch(InputMismatchException x) {
			System.out.println("Digite apenas n�meros");
		}

	}

}
