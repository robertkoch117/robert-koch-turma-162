import java.util.InputMismatchException;
import java.util.Scanner;

/**
 * 
 * @author Robert_Koch_2017101788
 * @version 1.0
 */
public class Ex5ComFuncao {
	static int n;
	
	public static void numero() {
		if(n>=0) {
			System.out.println("O n�mero � positivo.");
		}else {
			System.out.println("O n�mero � negativo.");
		}
	}
	
	public static void main(String[] args) {
		
		try {
			
			Scanner sc = new Scanner(System.in);
			
			System.out.print("Digite um n�mero: ");
			n = sc.nextInt();
			
			numero();
			
			sc.close();
		}catch(InputMismatchException x) {
			System.out.println("Digite apenas n�meros.");
		}
	}

}
