import java.util.Scanner;
import java.util.InputMismatchException;
/**
 * 
 * @author Robert_Koch_2017101788
 * @version 1.0
 *
 */
public class Ex1 {
	public static void main(String[] args) {
		//Exercicio 1 - Selecao
		try {
			int cod;
			Scanner sc = new Scanner(System.in);
			
			System.out.print("Escreva o c�digo de origem: ");
			cod = sc.nextInt();
			
			switch(cod){
				case 1:
					System.out.println("Sul");
					break;
				case 2:
					System.out.println("Norte");
					break;
				case 3:
					System.out.println("Leste");
					break;
				case 4:
					System.out.println("Oeste");
					break;
				case 5:
				case 6:
					System.out.println("Nordeste");
					break;
				case 7:
				case 8:
				case 9:
					System.out.println("Sudeste");
					break;
				case 10:
					System.out.println("Centro-Oeste");
					break;
				case 11:
					System.out.println("Noroeste");
					break;
				default:
					System.out.println("Importado");
			}
			sc.close();
		}catch(InputMismatchException x) {
			System.out.println("Digite apenas n�meros.");
		}
	}
}
	