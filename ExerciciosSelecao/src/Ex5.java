import java.util.Scanner;
import java.util.InputMismatchException;
/**
 * 
 * @author Robert_Koch_2017101788
 * @version 1.0
 */
public class Ex5 {
	public static void main(String[] args) {
		//Exercicio 5 - Selecao
		try {
			int n;
			Scanner sc = new Scanner(System.in);
			
			System.out.print("Digite um n�mero: ");
			n = sc.nextInt();
			
			if(n>=0) {
				System.out.println("O n�mero � positivo.");
			}else {
				System.out.println("O n�mero � negativo.");
			}
			sc.close();
		}catch(InputMismatchException x) {
			System.out.println("Digite apenas n�meros.");
		}
	}
}
