import java.util.InputMismatchException;
import java.util.Scanner;

/**
 * 
 * @author Robert_Koch_2017101788
 * @version 1.0
 *
 */
public class Ex3ComFuncao {

	public static double media(double n1, double n2){
		return (n1 + n2)/2; 
	}
	
	public static void main(String[] args) {
		try {
			double av1, av2;
			Scanner sc = new Scanner(System.in);
			
			System.out.print("Escreva a primeira nota: ");
			av1 = sc.nextInt();
			System.out.print("Escreva a segunda nota: ");
			av2 = sc.nextInt();
			
			if(media(av1,av2)>=6) {
				System.out.println("PARAB�NS! Voc� foi aprovado!");
			}
			sc.close();
		}catch(InputMismatchException x) {
			System.out.println("Digite apenas n�meros");
		}

	}

}
