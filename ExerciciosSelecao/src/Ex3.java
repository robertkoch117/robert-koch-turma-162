import java.util.Scanner;
import java.util.InputMismatchException;
/**
 * 
 * @author Robert_Koch_2017101788
 * @version 1.0
 *
 */
public class Ex3 {

	public static void main(String[] args) {
		//Exercicio 3 - Selecao
		try {
			double av1, av2, media;
			Scanner sc = new Scanner(System.in);
			
			System.out.print("Escreva a primeira nota: ");
			av1 = sc.nextInt();
			System.out.print("Escreva a segunda nota: ");
			av2 = sc.nextInt();
			media = (av1 + av2)/2;
			if(media>=6) {
				System.out.println("PARAB�NS! Voc� foi aprovado!");
			}
			sc.close();
		}catch(InputMismatchException x) {
			System.out.println("Digite apenas n�meros");
		}
	}
}
