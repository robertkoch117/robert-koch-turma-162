/**
 * 
 * @author Robert_Koch_2017101788
 * @version 1.0
 *
 */
public class Ex4 {

	public static void main(String[] args) {
		int v[] = {5,1,4,2,7,8,3,6};
		int aux;
		
		for(int i=7; i>4;i--) {
			aux = v[i];
			v[i] = v[7-i+1];
			v[7-i+1] = aux;
		}
		v[3] = v[1];
		v[v[3]] = v[v[2]];
		
		for(int i=0; i<8;i++) {
			System.out.println("Vetor ["+i+"]: "+v[i]);
		}
		

	}

}
