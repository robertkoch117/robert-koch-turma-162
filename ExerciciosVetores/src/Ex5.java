import java.util.Scanner;
/**
 * 
 * @author Robert_Koch_2017101788
 * @version 1.0
 *
 */
public class Ex5 {
	
	public static void main(String[] args) {
		int c[] = new int[10];
		Scanner sc = new Scanner(System.in);
		
		for(int i=0; i<10; i++) {
			System.out.print("Valor para Vetor["+ i +"]: ");
			c[i]= sc.nextInt();
			if(c[i]<0) {
				c[i]=0;
			}
		}
		
		System.out.println("\n");
		for(int i=0; i<10; i++) {
			System.out.println("Vetor["+i+"]: "+c[i]);
		}
		sc.close();
	}

}
