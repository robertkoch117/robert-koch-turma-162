/**
 * 
 * @author Robert_Koch_2017101788
 * @version 1.0
 *
 */
public class Ex6 {

	public static void main(String[] args) {
		
		int d[] = new int[10];
		int e[] = new int[10];
		
		//insercao dos valores
		for(int i=0; i<10; i++) {
			d[i] = i+1;
			e[9-i] = d[i];
		}
		
		//sa�da na tela
		//vetor d
		System.out.println("=========== Vetor D ===========");
		for(int i=0;i<10;i++) {
			System.out.println("Vetor ["+i+"]: "+d[i]);
		}
		
		//vetor e
		System.out.println("\n=========== Vetor E ===========");
		for(int i=0;i<10;i++) {
			System.out.println("Vetor ["+i+"]: "+e[i]);
		}
	}

}
