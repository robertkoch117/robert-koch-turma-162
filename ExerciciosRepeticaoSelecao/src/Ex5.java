import java.util.Scanner;
import java.util.InputMismatchException;
/**
 * @author Robert_Koch_2017101788
 * @version 1.0
 *
 */
public class Ex5 {

	public static void main(String[] args) {
		// Exercicios 5 - Repeticao
		
		try {
			double n1, n2, media;
			Scanner sc = new Scanner(System.in);
			
			//nota1
			do{
				System.out.print("Digite o valor da nota 1: ");
				n1 = sc.nextDouble();
				if(n1>0 && n1<10) {
					System.out.println("A nota 1 � v�lida");
				}else {
					System.out.println("Nota inv�lida!");
				}
			}while(n1<0 || n1>10);
			
			//nota2
			do{
				System.out.print("Digite o valor da nota 2: ");
				n2 = sc.nextDouble();
				if(n2>0 && n2<10) {
					System.out.println("A nota 2 � v�lida");
				}else {
					System.out.println("Nota inv�lida!");
				}
			}while(n2<0 || n2>10);
			
			media = (n1 + n2)/2;
			System.out.println("\nO valor da m�dia �: "+media);
			sc.close();
		} catch (InputMismatchException e) {
			System.out.println("Digite apenas n�meros.");
		}
	}

}
