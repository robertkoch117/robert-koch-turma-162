import java.util.InputMismatchException;
import java.util.Scanner;

/**
 * 
 * @author Robert_Koch_2017101788
 * @version 1.0
 *
 */
public class Ex5ComFuncao {
	
	public static void validacao(double n) {
		if(n>0 && n<10) {
			System.out.println("A nota "+ n +" � v�lida");
		}else {
			System.out.println("Nota inv�lida!");
		}
	}
	
	public static double media(double n1, double n2){
		return (n1+n2)/2;
	}
	
	public static void main(String[] args) {
		try {
			double n1, n2;
			
			Scanner sc = new Scanner(System.in);
			
			//nota1
			do{
				System.out.print("Digite o valor da nota 1: ");
				n1 = sc.nextDouble();
				validacao(n1);
			}while(n1<0 || n1>10);
			
			//nota2
			do{
				System.out.print("Digite o valor da nota 2: ");
				n2 = sc.nextDouble();
				validacao(n2);
			}while(n2<0 || n2>10);
			
			//finaliza��o do programa
			System.out.println("\nAs notas foram: "+ n1 + " e "+ n2);
			System.out.println("\nO valor da m�dia �: "+ media(n1,n2));
			sc.close();
		} catch (InputMismatchException e) {
			System.out.println("Digite apenas n�meros.");
		}
	}
}
