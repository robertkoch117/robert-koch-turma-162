import java.util.InputMismatchException;
import java.util.Scanner;

/**
 * 
 * @author Robert_Koch_2017101788
 * @version 1.0
 *
 */

public class Ex1 {

	public static void main(String[] args) {
		//Exercicio 1 - Repeticao
		try {
			double n1, n2, div;
			Scanner sc = new Scanner(System.in);
			
			do {
				System.out.print("\nEscreva o primeiro valor da divis�o: ");
				n1 = sc.nextDouble();
				System.out.print("\nEscreva o segundo valor da divis�o: ");
				n2 = sc.nextDouble();
		
				if(n2 == 0) {
					System.out.println("O segundo valor precisa ser diferente de zero!");
				}else {
					div = n1/n2;
					System.out.println("Resultado: " + div);
				}
			} while (n2 == 0);
			sc.close();
		}catch(InputMismatchException x){
			System.out.println("Digite apenas n�meros.");
		}
	}

}
