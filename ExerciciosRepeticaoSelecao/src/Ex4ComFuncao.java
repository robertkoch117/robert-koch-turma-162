import java.util.InputMismatchException;
import java.util.Scanner;

/**
 * 
 * @author Robert_Koch_2017101788
 * @version 1.0
 *
 */
public class Ex4ComFuncao {
	static double n1, n2=0, div;
	
	public static void comparar() {
		if(n2 == 0) {
			System.out.println("Valor inv�lido!");
		}else {
			div = n1/n2;
			System.out.println("Resultado: " + div);
		}
	}
	
	public static void main(String[] args) {
		try {
			
			Scanner sc = new Scanner(System.in);
						
			while(n2 == 0){
				System.out.print("Escreva o primeiro valor: ");
				n1 = sc.nextDouble();
				System.out.print("Escreva o segundo valor: ");
				n2 = sc.nextDouble();
				
				comparar();
			}
			sc.close();
		} catch (InputMismatchException x){
			System.out.println("Digite apenas n�meros.");
		}

	}

}
