import java.util.InputMismatchException;
import java.util.Scanner;

/**
 * 
 * @author Robert_Koch_2017101788
 * @version 1.0
 */
public class Ex3ComFuncao {
	static double n1, n2=0, div;
	
	public static double divisao(double n1, double n2) {
		return div = n1/n2;
	}
	
	public static void main(String[] args) {
		try {
			
			Scanner sc = new Scanner(System.in);
						
			while(n2 == 0){
				System.out.print("Escreva o primeiro valor: ");
				n1 = sc.nextDouble();
				System.out.print("Escreva o segundo valor: ");
				n2 = sc.nextDouble();
				if(n2 == 0) {
					System.out.println("O segundo valor precisa ser diferente de zero!");
				}else {
					System.out.println("Resultado: " + divisao(n1,n2));
				}
			}
			sc.close();
		} catch (InputMismatchException x){
			System.out.println("Digite apenas n�meros.");
		}
	}
}
