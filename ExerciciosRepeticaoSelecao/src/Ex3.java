import java.util.Scanner;
import java.util.InputMismatchException;
/**
 * 
 * @author Robert_Koch_2017101788
 * @version 1.0
 *
 */
public class Ex3{
	public static void main(String[] args){
		//Exercicio 3 - Repeticao
		try {
			double n1, n2=0, div;
			Scanner sc = new Scanner(System.in);
						
			while(n2 == 0){
				System.out.print("Escreva o primeiro valor: ");
				n1 = sc.nextDouble();
				System.out.print("Escreva o segundo valor: ");
				n2 = sc.nextDouble();
				if(n2 == 0) {
					System.out.println("O segundo valor precisa ser diferente de zero!");
				}else {
					div = n1/n2;
					System.out.println("Resultado: " + div);
				}
			}
			sc.close();
		} catch (InputMismatchException x){
			System.out.println("Digite apenas n�meros.");
		}
	}
}
