import java.text.DecimalFormat;
import java.util.InputMismatchException;
import java.util.Scanner;

/**
 * 
 * @author Robert_Koch_2017101788
 * @version	1.0
 *
 */

public class Ex1ComFuncao {
	static double raio, resultado;
	
	public static double area(double raio){
		resultado = Math.PI*(raio*raio);
		return resultado;
	}
	
	public static void main(String[] args) {
		//Exercicio 1 - Simples
			try {
				
				Scanner sc = new Scanner(System.in);
				DecimalFormat df = new DecimalFormat("#0.00");
				
				System.out.print("Insira o valor do raio: ");
				raio = sc.nextDouble();
				
				System.out.println("O valor da �rea da circunfer�ncia �: " + df.format(area(raio)));
				sc.close();
			}catch(InputMismatchException x){
				System.out.println("Digite apenas n�meros.");
			}
	}
}
