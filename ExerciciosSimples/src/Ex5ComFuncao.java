import java.util.InputMismatchException;
import java.util.Scanner;

/**
 * 
 * @author Robert_Koch_2017101788
 * @version 1.0
 */

public class Ex5ComFuncao {
	static double comp, larg, alt, cx, area;
	
	public static double qtdCaixas(double comp, double larg, double alt){
		area = (comp*alt*2) + (larg*alt*2);
		return cx = Math.round(area/1.5);
	}
	
	public static void main(String[] args) {
		try {
			
			Scanner sc = new Scanner(System.in);
			
			System.out.print("Escreva o comprimento: ");
			comp = sc.nextDouble();
			System.out.print("Escreva a largura: ");
			larg = sc.nextDouble();
			System.out.print("Escreva a altura: ");
			alt = sc.nextDouble();
			
			System.out.println("Quantidade de caixas de azulejos para colocar em todas as paredes: " + qtdCaixas(comp, larg, alt));
			sc.close();
			}catch(InputMismatchException x){
				System.out.println("Digite apenas n�meros.");
			}

	}

}
