import java.util.Scanner;
import java.text.DecimalFormat;
import java.util.InputMismatchException;
/**
 * 
 * @author Robert_Koch_2017101788
 * @version 1.0
 */
public class Ex3 {
	public static void main(String[] args) {
		//Exercicio 3 - Simples
		try {
			double c, f;
			Scanner sc = new Scanner(System.in);
			DecimalFormat df = new DecimalFormat("#0.0");
			
			System.out.print("Temperatura em Celsius: ");
			c = sc.nextDouble();
			f = c * 1.8 + 32;
			System.out.println("Temperatura em Fahrenheit: " + df.format(f));
			sc.close();
		} catch (InputMismatchException x) {
			System.out.println("Digite apenas n�meros.");
		}
	}	
}
