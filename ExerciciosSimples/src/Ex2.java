import java.util.Scanner;
import java.util.InputMismatchException;
import java.text.DecimalFormat;
/**
 * 
 * @author Robert_Koch_2017101788
 * @version 1.0
 *
 */
public class Ex2 {
	public static void main(String[] args) {
		//Exercicio 2 - Simples
		try {
			double f, c;
			Scanner sc = new Scanner(System.in);
			DecimalFormat df = new DecimalFormat("#0.000");
			
			System.out.print("Temperatura em Fahrenheit: ");
			f = sc.nextDouble();
			c = (f - 32)/ 1.8;
			System.out.println("Temperatura em Celsius: " + df.format(c));
			sc.close();
		}catch(InputMismatchException x){
			System.out.println("Digite apenas números.");
		}
	}
}
