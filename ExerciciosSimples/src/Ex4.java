import java.util.Scanner;
import java.util.InputMismatchException;
/**
 * 
 * @author Robert_Koch_2017101788
 * @version 1.0 
 * 
 */
public class Ex4 {
	public static void main(String[] args) {
		//Exerc�cio 4 - Simples
		try {
			double pot_lamp, pot_total, larg, comp, num_lamp, area;
			Scanner sc = new Scanner(System.in);
			
			System.out.print("Pot�ncia da l�mpada: ");
			pot_lamp = sc.nextDouble();
			System.out.print("\nLargura do c�modo: ");
			larg = sc.nextDouble();
			System.out.print("\nComprimento do c�modo: ");
			comp = sc.nextDouble();
			
			area = larg * comp;
			pot_total = area * 18;
			num_lamp = Math.round(pot_total / pot_lamp);
			
			System.out.println("N�mero total de l�mpadas: " + num_lamp);
			sc.close();
		}catch(InputMismatchException x){
			System.out.println("Digite apenas n�meros.");
		}
	}
}
