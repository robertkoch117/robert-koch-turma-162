import java.text.DecimalFormat;
import java.util.InputMismatchException;
import java.util.Scanner;

/**
 * 
 * @author Robert_Koch_2017101788
 * @version 1.0
 */

public class Ex2ComFuncao {
	static double f, c;
	
	public static double conversor(double f){
		return c = (f-32)/1.8;
	}
	
	public static void main(String[] args) {
		try {
			
			Scanner sc = new Scanner(System.in);
			DecimalFormat df = new DecimalFormat("#0.000");
			
			System.out.print("Temperatura em Fahrenheit: ");
			f = sc.nextDouble();
			
			System.out.println("Temperatura em Celsius: " + df.format(conversor(f)));
			sc.close();
			
		}catch(InputMismatchException x){
			System.out.println("Digite apenas n�meros.");
		}
	}
}
