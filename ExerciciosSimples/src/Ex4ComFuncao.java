import java.util.InputMismatchException;
import java.util.Scanner;

public class Ex4ComFuncao {
	static double pot_lamp, pot_total, larg, comp, num_lamp, area;
	
	public static double numLamp(double pot_lamp, double larg, double comp) {
		area = larg * comp;
		pot_total = area * 18;
		num_lamp = Math.round(pot_total / pot_lamp);
		return num_lamp;
	}
	
	public static void main(String[] args) {
		try {
			
			Scanner sc = new Scanner(System.in);
			
			System.out.print("Pot�ncia da l�mpada: ");
			pot_lamp = sc.nextDouble();
			System.out.print("\nLargura do c�modo: ");
			larg = sc.nextDouble();
			System.out.print("\nComprimento do c�modo: ");
			comp = sc.nextDouble();
			
			System.out.println("N�mero total de l�mpadas: " + numLamp(pot_lamp,larg,comp));
			sc.close();
		}catch(InputMismatchException x){
			System.out.println("Digite apenas n�meros.");
		}

	}

}
