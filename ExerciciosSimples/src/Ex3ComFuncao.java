import java.text.DecimalFormat;
import java.util.InputMismatchException;
import java.util.Scanner;

/**
 * 
 * @author Robert_Koch_2017101788
 * @version 1.0
 */

public class Ex3ComFuncao {
	static double f;
	
	public static double conversao(double c) {
		return f = c * 1.8 + 32;
	}
	
	public static void main(String[] args){
		try {
			double c;
			Scanner sc = new Scanner(System.in);
			DecimalFormat df = new DecimalFormat("#0.0");
			
			System.out.print("Temperatura em Celsius: ");
			c = sc.nextDouble();
			
			System.out.println("Temperatura em Fahrenheit: " + df.format(conversao(c)));
			sc.close();
		} catch (InputMismatchException x) {
			System.out.println("Digite apenas n�meros.");
		}
	}
}
