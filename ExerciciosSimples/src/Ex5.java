import java.util.Scanner;
import java.util.InputMismatchException;
/**
 * 
 * @author Robert_Koch_2017101788
 * @version 1.0
 *
 */

public class Ex5 {
	public static void main(String[] args){
		//Exerc�cio 5 - Simples
		try {
		double comp, larg, alt, cx, area;
		Scanner sc = new Scanner(System.in);
		
		System.out.print("Escreva o comprimento: ");
		comp = sc.nextDouble();
		System.out.print("Escreva a largura: ");
		larg = sc.nextDouble();
		System.out.print("Escreva a altura: ");
		alt = sc.nextDouble();
		
		area = (comp*alt*2) + (larg*alt*2);
		cx = Math.round(area/1.5);
		
		System.out.println("Quantidade de caixas de azulejos para colocar em todas as paredes: " + cx);
		sc.close();
		}catch(InputMismatchException x){
			System.out.println("Digite apenas n�meros.");
		}
	}
}
