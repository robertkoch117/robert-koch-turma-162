import java.util.Scanner;
import java.text.DecimalFormat;
import java.util.InputMismatchException;

/**
 * 
 * @author Robert_Koch_2017101788
 * @version 1.0
 *
 */

public class Ex1 {
	public static void main(String[] args) {
		//Exercicio 1 - Simples
		try {
			double raio, area;
			Scanner sc = new Scanner(System.in);
			DecimalFormat df = new DecimalFormat("#0.00");
			
			System.out.print("Insira o valor do raio: ");
			raio = sc.nextDouble();
			area = Math.PI*(raio*raio);
			System.out.println("O valor da �rea da circunfer�ncia �: " + df.format(area));
			sc.close();
		}catch(InputMismatchException x){
			System.out.println("Digite apenas n�meros.");
		}
	}
}
