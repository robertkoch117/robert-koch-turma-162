package question2;

/**
 * 
 * @author Robert_Koch_2017101788
 * @version 1.0
 * 
 *
 */
abstract public class Conta {
	private long numConta;
	private double saldo;
	
	//Construtor
	public Conta(long numConta, double saldo){
		this.numConta = numConta;
		this.saldo = saldo;
	}
	
	//M�todos de Acesso
	public long getNumConta() {
		return numConta;
	}
	
	public void setNumConta(long numConta) {
		this.numConta = numConta;
	}
	
	public double getSaldo() {
		return saldo;
	}
	
	public void setSaldo(double saldo) {
		this.saldo = saldo;
	}
	
	//M�todos de neg�cio
	public void debitar(double soma) {
		saldo -= soma;
	}
	
	public void creditar(double soma) {
		saldo += soma;
	}
	
}
