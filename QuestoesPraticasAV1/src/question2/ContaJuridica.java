package question2;

/**
 * 
 * @author Robert_Koch
 * @version 1.0
 *
 */
public class ContaJuridica extends Conta{
	private long cnpj;
	
	//Construtor
	public ContaJuridica(long numConta, double saldo, long cnpj) {
		super(numConta, saldo);
		this.cnpj = cnpj;
	}
	
	//Metodos de Acesso
	public long getCnpj() {
		return cnpj;
	}
	
	public void setCnpj(long cnpj) {
		this.cnpj = cnpj;
	}
	
}
