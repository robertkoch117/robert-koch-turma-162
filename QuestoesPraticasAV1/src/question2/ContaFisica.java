package question2;
/**
 * 
 * @author Robert_Koch_2017101788
 * @version 1.0
 *
 */
public class ContaFisica extends Conta{
	private long cpf;
	
	//Construtor
	public ContaFisica(long numConta, double saldo, long cpf) {
		super(numConta, saldo);
		this.cpf = cpf;
	}
	
	//M�todos de Acesso
	public long getCpf() {
		return cpf;
	}
	
	public void setCpf(long cpf) {
		this.cpf = cpf;
	}
	
}
