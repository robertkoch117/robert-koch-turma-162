package question2;

/**
 * @author Robert_Koch_2017101788
 * @version 1.0
 * 
 */
import java.util.Scanner;

public class Q2 {

	//Constante
	static final int max = 10;
	
	//vari�vel comum
	static int index = 0;
	
	//lista de contas
	static Conta[] lista = new Conta[max];
	
	//variavel de entrada de valores
	static Scanner sc = new Scanner(System.in);
	
	public static void main(String[] args) {
		int op;
		try {
			
			do {
				System.out.println("---------MENU---------");
				System.out.println("1- Inserir nova conta");
				System.out.println("2- Excluir conta");
				System.out.println("3- D�bito");
				System.out.println("4- Cr�dito");
				System.out.println("5- Consultar saldo");
				System.out.println("6- Transfer�ncia");
				System.out.println("0- Sair");
				System.out.println("----------------------");
				System.out.print("Efetue a sua escolha: ");
				op = sc.nextInt();
				switch(op) {
					case 1:	inserirConta(); break;
					case 2:	excluirConta(); break;
					case 3:	debitoConta(); break;
					case 4: creditoConta(); break;
					case 5: consultarConta(); break;
					case 6:	transferenciaConta(); break;
					case 0:	break;
				}
			}while(op!=0);
		}catch(Exception e ){
			System.out.println("Ocorreu um erro no sistema");
		}
		
	}
	

/**
 * @category Fun��o para inserir conta
 * @param none;
 * 
 */
	public static void inserirConta() {
		
		System.out.print("Insira o n�mero da conta: ");
		long numConta = sc.nextLong();
		System.out.print("Insira o valor do saldo: ");
		double saldo = sc.nextDouble();
		System.out.println("-----------------------\n"
						   + "Qual � o tipo da conta?"); //Fisica ou Juridica??
		System.out.println("1-Fisica\n"
						 + "2-Juridica\n"
						 + "-----------------------");
		int op = sc.nextInt();
		
		switch(op) { //case para escolher entre Fisica ou Juridica
			case 1: 
				System.out.print("Insira o cpf: ");
				long cpf = sc.nextLong(); 
				//Criar o objeto e inserir na lista
				lista[index++] = new ContaFisica(numConta, saldo, cpf);
				break;
			case 2:
				System.out.print("Insira o cnpj: ");
				long cnpj = sc.nextInt(); 
				//Criar o objeto e inserir na lista
				lista[index++] = new ContaJuridica(numConta, saldo, cnpj);
				break;
			default:
				System.out.println("N�o foi poss�vel prosseguir com essa escolha.");
		}
		System.out.println("Conta cadastrada com sucesso!");
	}
	
	
	/**
	 * Fun��o para consultar conta inserindo o numero dela, e indicando caso n�o seja encontrada
	 * 
	 */
	private static void consultarConta() {
		boolean encontrado=false; //vari�vel para validar se a conta foi encontrada ou n�o
		
		System.out.print("Insira o n�mero da conta: ");
		long numConta = sc.nextLong();
		
		for(int i=0; i<lista.length-1; i++) {
			if(lista[i] != null) { //valida se � null ou n�o
				if(lista[i].getNumConta()==numConta){ //caso valor do numero da conta seja o mesmo indicado pelo usuario o codigo � executado
					System.out.println("--------------------\n"
									 + "N�mero da Conta: "+ lista[i].getNumConta()+"\nSaldo: "+lista[i].getSaldo());
					System.out.println("--------------------");
					encontrado=true;
				}
			}else { //caso for null sai da itera��o e parte para a pr�xima
				break;
			}
		}
		if(encontrado != true) {
			System.out.println("Conta n�o encontrada.");
		}
	}
	
	/**
	 * Exclui uma conta caso ela exista, e informa caso n�o seja encontrada
	 */
	public static void excluirConta() {
		boolean encontrado = false;
		System.out.print("Insira o N�mero da Conta: ");
		long numConta = sc.nextLong();
		
		for(int i=0; i<lista.length-1;i++) {
			if(lista[i]!=null) {
				if(lista[i].getNumConta() == numConta) {
					lista[i].setNumConta(0);
					lista[i].setSaldo(0);
					encontrado = true;
					System.out.println("Conta exclu�da com sucesso!");
				}
			}else {
				break;
			}
		}
		if(encontrado != true) {
			System.out.println("A conta n�o foi encontrada!");
		}
	}
	/**
	 * Debitar em uma conta existente
	 */
	public static void debitoConta() {
		boolean achou=false;
		
		System.out.print("Insira o n�mero da conta: ");
		long numConta = sc.nextLong();
		System.out.print("Insira o valor a ser debitado: ");
		double debito = sc.nextDouble();
		for(int i=0; i<lista.length-1; i++) {
			if(lista[i]!=null) {
				if(lista[i].getNumConta()==numConta) {
					lista[i].debitar(debito);
					achou = true;
					System.out.println("D�bito realizado com sucesso!");
				}
			}else {
				break;
			}
		}
		if(achou != true) {
			System.out.println("A conta "+ numConta + " n�o foi encontrada!");
		}
	}
	
	/**
	 * Adicionar creditos em uma conta j� existente
	 */
	public static void creditoConta() {
		boolean achou = false;
		System.out.print("Insira o n�mero da conta: ");
		long numConta = sc.nextLong();
		System.out.print("Valor a ser creditado: ");
		double credito = sc.nextDouble();
		
		for(int i=0; i<lista.length-1; i++) {
			if(lista[i]!=null) {
				if(lista[i].getNumConta() == numConta) {
					lista[i].creditar(credito);
					achou = true;
					System.out.println("Cr�dito feito com sucesso!");
				}
			}else {
				break;
			}
		}
		if(!achou) {
			System.out.println("A conta "+ numConta +" n�o foi encontrada");
		}
	}
	
	/**
	 * Transferir dinheiro de uma conta para outra
	 */
	private static void transferenciaConta() {
		System.out.print("N�mero da Conta (Origem): ");
		long contaOrigem = sc.nextLong();
		System.out.print("N�mero da Conta (Destino): ");
		long contaDestino = sc.nextLong();
		System.out.print("Valor da transfer�ncia: ");
		double valor = sc.nextLong();
		
		for(int i=0; i<lista.length-1; i++) {
			if(lista[i]!=null) {
				if(lista[i].getNumConta()==contaOrigem) {
					lista[i].debitar(valor);
					System.out.println("O valor "+valor+" foi debitado da conta "+ contaOrigem+".");
				}
				if(lista[i].getNumConta()==contaDestino){
					lista[i].creditar(valor);
					System.out.println("O valor "+valor+" foi creditado a conta "+contaDestino+".");
				}
			}else {
				break;
			}
		}
	}
}
