import java.util.Scanner;

/**
 * 
 * @author Robert_Koch_2017101788
 * @version 1.0
 *
 */
public class Q1 {
	
	//variaveis
	static int op,index, tam=10;
	static Scanner sc1 = new Scanner(System.in); //numeros
	static Scanner sc2 = new Scanner(System.in); //strings
	static Pessoa lista[] = new Pessoa[tam];
	
	/**
	 * @author Robert_Koch
	 * @category criar o menu inicial para as escolhas das op��es
	 * @param nenhum
	 * 
	 */
	public static void menu() {
		System.out.println("*----------Menu----------*");
		System.out.println("|(1) Inserir             |");
		System.out.println("|(2) Listar              |");
		System.out.println("|(3) Sair                |");
		System.out.println("*------------------------*");
	}
	
	/**
	 * @author Robert_Koch
	 * @category inserir pessoas na lista
	 * @param void
	 */
	public static void inserir() {
		System.out.println("");
		
		System.out.print("Escreva o nome: ");
		String nome = sc2.nextLine();
		System.out.print("\nEscreva o CPF: ");
		String cpf = sc2.nextLine();
		lista[index++] = new Pessoa(nome,cpf);
		System.out.println("Pessoa inserida com sucesso!");
	}
	
	/**
	 * @author Robert_Koch
	 * @category listar as pessoas inseridas dentro da lista
	 * @param nenhum
	 */
	public static void listar() {
		System.out.println("");
		for (int i=0; i<lista.length-1; i++) {
			if(lista[i]!=null) {
				System.out.println(lista[i].getNome()+".........."+lista[i].getCpf());
			}else {
				break;
			}
		}
	}
	
	public static void main(String[]args) {
		try {
			//estrutura de repeticao
			do {
				//iniciando o menu de escolha
				menu();
				System.out.print("Escolha uma op��o: ");
				op = sc1.nextInt();
	
				//estrutura de sele��o
				switch(op) {
					case 1:
						inserir();
						break;
					case 2:
						listar();
						break;
					case 3:
						break;
				}
			}while(op!=3);
			
		} catch (Exception e) {
			// TODO: handle exception
		}
	}
}
