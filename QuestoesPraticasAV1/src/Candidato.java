
public class Candidato {
	
	private int numero;
	private String nome;
	private int votos;
	
	/**
	 * @category Construtor com dois parametros 
	 * @param numero
	 * @param nome
	 */
	public Candidato(int numero, String nome) {
		this.nome = nome;
		this.numero = numero;
		votos = 0;
	}
	
	public int getNumero() {
		return numero;
	}
	
	public void setNumero(int numero) {
		this.numero = numero;
	}
	
	public String getNome() {
		return nome;
	}
	
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	public int getVotos() {
		return votos;
	}
	
	public void setVotos() {
		this.votos++;
	}
	
	/*public void votar() {
		this.votos++;
	}*/
}
