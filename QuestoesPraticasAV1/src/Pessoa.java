/**
 * 
 * @author Robert_Koch_2017101788
 * @version 1.0
 *
 */
public class Pessoa {
	private String nome;
	private String cpf;
	private int matricula;
	
	//Construtor
		public Pessoa(String nome, String cpf) {
			this.nome = nome;
			this.cpf = cpf;
		}
	
	//M�todos Especiais
	public String getNome() {
		return nome;
	}
	
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	public String getCpf() {
		return cpf;
	}
	
	public void setCpf(String cpf) {
		this.cpf = cpf;
	}
	
	public int getMatricula() {
		return matricula;
	}
	
	public void setMatricula(int matricula) {
		this.matricula = matricula;
	}
	
	
	
}
