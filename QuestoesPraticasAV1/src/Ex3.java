import java.util.Scanner;

/**
 * 
 * @author Robert_Koch_2017101788
 * @version 1.0
 */
public class Ex3 {
	
	//Constante
	static final int MAXCONTA = 4;
	
	//variavel comum
	static int index = 0;
	
	//Lista de contas
	static Candidato[] lista = new Candidato[MAXCONTA];
	
	static Scanner tecla = new Scanner(System.in);
	
	public static void main(String[] args) {
		try {
			int op;
			do {
			System.out.println("*----------Menu----------*");
			System.out.println("|(1) Incluir Candidato   |");
			System.out.println("|(2) Votar               |");
			System.out.println("|(3) Apura��o            |");
			System.out.println("|(4) Sair                |");
			System.out.println("*------------------------*");
			op = tecla.nextInt();
			switch(op) {
				case 1:
					incluirCandidato();
					break;
				case 2:
					votarCandidato();
					break;
				case 3:
					apurarVotos();
					break;
				case 4:
					break;
			}
			}while(op!=4);
		} catch (Exception e) {
			// TODO: handle exception
		}

	}

	private static void apurarVotos() {
		for(int i=0; i<lista.length-1; i++) {
			if(lista[i]!= null) {
				
			}else {
				break;
			}
		}
		
	}

	private static void votarCandidato() {
		System.out.println("Digite o n�mero do candidato: ");
		int num = tecla.nextInt();
		
		//Procurar a conta na Lista
		for (int i=0; i<lista.length-1;i++) {
			if(num == lista[i].getNumero()) {
				lista[i].setVotos();
				break;
			}
		}
	}

	private static void incluirCandidato() {
		System.out.println("Digite o n�mero do candidato: ");
		int num = tecla.nextInt();
		System.out.println("Digite o nome do candidato: ");
		String nome = tecla.nextLine();
		//Criar o objeto e inserir na lista
		lista[index++] = new Candidato(num, nome);
		System.out.println("Candidato cadastrado com sucesso!");
		
	}

}
